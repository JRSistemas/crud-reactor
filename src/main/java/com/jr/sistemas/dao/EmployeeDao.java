package com.jr.sistemas.dao;

import com.jr.sistemas.documents.Employee;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface EmployeeDao extends ReactiveMongoRepository<Employee,String> {
}
