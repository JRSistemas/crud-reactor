package com.jr.sistemas.services.impl;

import com.jr.sistemas.dao.EmployeeDao;
import com.jr.sistemas.documents.Employee;
import com.jr.sistemas.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeDao employeeDao;
    
    @Override
    public Flux<Employee> findAll() {
        return employeeDao.findAll();
    }

    @Override
    public Flux<Employee> findAllWithNameUpperCase() {
        return employeeDao.findAll().map(emp -> {
            emp.setFirstName(emp.getFirstName().toUpperCase());
            emp.setLastName(emp.getLastName().toUpperCase());
            return emp;
        });
    }
    @Override
    public Flux<Employee> findAllWithNameUpperCaseRepeat() {
        return findAllWithNameUpperCase().repeat(500);
    }

    @Override
    public Mono<Employee> findById(String id) {
        return employeeDao.findById(id);
    }

    @Override
    public Mono<Employee> save(Employee employee) {
        return employeeDao.save(employee);
    }

    @Override
    public Mono<Void> delete(Employee employee) {
        return employeeDao.delete(employee);
    }
}
