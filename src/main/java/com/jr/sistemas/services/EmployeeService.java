package com.jr.sistemas.services;

import com.jr.sistemas.documents.Employee;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface EmployeeService {
    Flux<Employee> findAll();
    Flux<Employee> findAllWithNameUpperCase();
    Flux<Employee> findAllWithNameUpperCaseRepeat();
    Mono<Employee> findById(String id);
    Mono<Employee> save(Employee employee);
    Mono<Void> delete(Employee employee);
}
