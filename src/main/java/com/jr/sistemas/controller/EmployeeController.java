package com.jr.sistemas.controller;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import com.jr.sistemas.documents.Employee;
import com.jr.sistemas.services.EmployeeService;
import jakarta.validation.Valid;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SessionAttributes("employee")
@Controller
public class EmployeeController
{
    private static final Logger log = LoggerFactory.getLogger(EmployeeController.class);
    @Autowired
    private EmployeeService service;

    @GetMapping("/list")
    public Mono<String> getAll(Model model){
        Flux<Employee> employees = service.findAllWithNameUpperCase();
        employees.subscribe(emp -> log.info(String.format("FirstName: %s , LastName: %s ",emp.getFirstName()(),emp.getLastName())));

        model.addAttribute("employees", employees);
        model.addAttribute("titulo", "List of employees");

        return Mono.just("list");
    }

    @GetMapping("/form")
    public Mono<String> crear(Model model){
        model.addAttribute("employee", new Employee());
        model.addAttribute("titulo", "Form of Employee");
        model.addAttribute("boton", "Save");
        return Mono.just("form");
    }

    @PostMapping("/form")
    public Mono<String> guardar(@Valid Employee employee, BindingResult result,Model model ,SessionStatus status)
    {
    	if(result.hasErrors()) {
    		model.addAttribute("titulo", "Errores en el formulario employee.");
    		model.addAttribute("boton", "Save");
    		return Mono.just("form");
    	}else {
    		status.setComplete();
    		if(employee.getCreateAt() == null) {
    			employee.setCreateAt(new Date());
    		}
    		return service.save(employee).doOnNext(p ->{
    			log.info("FirstName: "+ p.getFirstName()+" Id: "+ p.getId());
    		}).then(Mono.just("redirect:/list?success=empleado+guardado+con+exito."));
    	}
    		
    		
    }

    @GetMapping("/form/{id}")
    public Mono<String> editar(@PathVariable String id, Model model){
        Mono<Employee> monoEmployee = service.findById(id).doOnNext(p->{
           log.info(String.format("FirstName: %s , LastName: %s",p.getFirstName(),p.getLastName()));
        }).defaultIfEmpty(new Employee());

        model.addAttribute("titulo", "Edit Employee");
        model.addAttribute("employee", monoEmployee);
        model.addAttribute("boton", "Edit");
        return Mono.just("form");
    }
    
    @GetMapping("/eliminar/{id}")
    public Mono<String> eliminar(@PathVariable String id)
    {
    	return service.findById(id)
    			.defaultIfEmpty(new Employee())
    			.flatMap( p -> {
    				if(p.getId() == null) {
    					return Mono.error(new InterruptedException("No existe el employee a eliminar"));
    				}
    				return Mono.just(p);
    			})
    			.flatMap(p ->{
    				log.info("Eliminando employee, firstName: "+p.getFirstName());
    				log.info("Eliminando employee, id: "+ p.getId());
    				return service.delete(p);
    			}).then(Mono.just("redirect:/list?success=employee+eliminado+con+exito."))
    			.onErrorResume(ex -> Mono.just("redirect:/list?error=no+existe+el+employee"));
    }
}





